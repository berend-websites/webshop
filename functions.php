<?php

// Globale variablen
$gebruikers_table = 'gebruikers';
$artikelen_table = 'artikelen';
$linked_table = 'linked_artikelen';
$database = 'webshop';

/* --> Connecten naar Database
* Args: None.
* Returns: Database connection ($con).
*/
function connect_db() {

        $host = 'localhost';
        $user = 'root';
        $ww = '';
        $database = 'webshop';

        $con = mysql_connect($host, $user, $ww);

        return $con;
}

/* --> Select Query opbouwen
* Args: Tabel naam, Collunm Naam, Value to filter.
* Retruns: SELECT SQL Query.
*/
function select_query($table, $col, $value) {
        $query 	= "SELECT * FROM $table WHERE $col='$value'";

        return $query;
}

/* --> Select all Query opbouwen
* Args: Tabel naam.
* Retruns: SELECT SQL Query.
*/
function selectall_query($table) {
        $query = "SELECT * FROM $table";
        
        return $query;
}

/* --> Insert Gebruiker Query opbouwen.
* Args: Naam, Wachtwoord, Admin (1/0)
* Returns: INSERT SQL Query.
*/
function insert_gebruiker_query($naam, $wachtwoord, $admin) {
        $query = "INSERT INTO gebruikers (naam, wachtwoord, admin) VALUES ('$naam', '$wachtwoord', $admin)";

        return $query;
}

/* --> Check of gebruiker is ingelogd
* Args: Geen.
* Returns: Als gebruiker is ingelogd -> true.
*/
function islogin() {
        if(isset($_SESSION['user'])) {
                return true;
        }
}

/* --> Check of gebruiker admin is.
* Args: Geen
* Returns: Als gebruiker admin is -> true;
*/

function isadmin() {
        $user = $_SESSION['user'];
        $con = connect_db();
        $database = 'webshop';
        mysql_select_db($database);
        $query = "SELECT * FROM gebruikers WHERE naam='$user' AND admin=1";
        $result = mysql_query($query);
        if(mysql_num_rows($result) > 0) {
                return true;
        }
        mysql_close($query);
} 
?>
