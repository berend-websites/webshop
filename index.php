<?php
	session_start();

	include_once('functions.php');

        $loginfo = '';
        $tabs = '';

        if(islogin()) {
                $loginfo = "Ingelogd als " . $_SESSION['user'] . " - <a class='logout' href='logout.php'>Log out</a>";
                if(isadmin()) {
                        $tabs = "<ul><a class='tabs' target='pagina' href='homepage.html'>Home Page</a>
                                 </ul><ul><a class='tabs' target='pagina' href='artikelen.php'>Artikelen</a></ul>
                                 <ul><a class='tabs' target='pagina' href='wikelwagen.php'>Winkelwagen</a></ul>  
                                 <ul><a class='tabs' target='pagina' href='gebruikers.php'>Gebruikers</a></ul>
                                 <ul><a class='tabs' target='pagina' href='addartikel.php'>Artiekel toevoegen</a></ul>";
                }
                else {
                     $tabs = "<ul><a class='tabs' target='pagina' href='homepage.html'>Home Page</a></ul>
                              <ul><a class='tabs' target='pagina' href='artikelen.php'>Artikelen</a></ul>
                              <ul><a class='tabs' target='pagina' href='wikelwagen.php'>Winkelwagen</a></ul>";   
                }
        }
        else {
                $loginfo = "<a class='logout' href='login.php'>Login</a> or 
                            <a class='logout' href='register.php' target='pagina'>register</a>";
                $tabs = "<ul><a class='tabs' target='pagina' href='homepage.html'>Home Page</a>
                        </ul><ul><a class='tabs' target='pagina' href='artikelen.php'>Artikelen</a></ul>";
        }
?>

<!DOCTYPE HTML>
<html>
<head>
        <title>WebShop</title>
        <link rel='stylesheet' type='text/css' href='css/index.css' />
</head>
<body>
<center>
<div id='aboveheader'>
<div id='log'>
<?php echo $loginfo; ?>
</div>
</div>
<div id='header'> 
        <span class='titel'>WebShop</span><br>
        <span class='webshopinfo'>Voor Al uw mobiele producten</span>
</div>
<div id='tabs'>
        <li>
               <?php echo $tabs; ?>
        </li>
</div>
<div id='undertabs'>
</div>
<div id='page'>
        <div id='headpage'>
                
        </div>
        <iframe width='810' height='480' name='pagina' src='homepage.html'> </iframe>
</div>
<div id='footer'>
        Berend de Groot & Russell Zweers - HTML4 - PHP5 - CSS2
</div>
</center>
</body>
</html>
